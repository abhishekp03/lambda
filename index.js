const AWS = require("aws-sdk");

const dynamo = new AWS.DynamoDB.DocumentClient();

exports.handler = async (event, context) => {
  let body;
  let statusCode = 200;
  const headers = {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*"
  };

  try {
    console.log(event);
    const key = `${event.httpMethod} ${event.resource}`
    switch (key) {
      case "GET /profile/{userId}":
        body = await dynamo
          .get({
            TableName: "profile_details",
            Key: {
              user_id: event.pathParameters.userId
            }
          })
          .promise();
        break;
      case "PUT /profile":
        let requestJSON = JSON.parse(event.body);
        await dynamo
          .put({
            TableName: "profile_details",
            Item: {
              user_id: requestJSON.userId,
              first_name: requestJSON.firstName,
              last_name: requestJSON.lastName
            }
          })
          .promise();
        body = `Profile details saved!`;
        break;
      default:
        throw new Error(`Unsupported route: "${event.routeKey}"`);
    }
  } catch (err) {
    statusCode = 400;
    body = err.message;
  } finally {
    body = JSON.stringify(body);
  }

  return {
    statusCode,
    body,
    headers
  };
};